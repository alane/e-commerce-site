<?php
$pageTitle = "T-shirts";
include("inc/header.php"); ?>

		<div class="section banner">

			<div class="wrapper">

				<img class="hero" src="#" alt="#">
				<div class="button">
					<a href="#">
						<h2>Hello!</h2>
						<p>Check Out My Shirts</p>
					</a>
				</div>
			</div>

		</div>

		<div class="section shirts latest">

			<div class="wrapper">

				<h2>Latest Shirts</h2>

				<ul class="products">
					<li><a href="#">
							<img src="img/shirts/shirt-108.jpg">
							<p>View Details</p>
						</a>
					</li><li>
						<a href="#">
							<img src="img/shirts/shirt-107.jpg">
							<p>View Details</p>
						</a>
					</li><li>
						<a href="#">
							<img src="img/shirts/shirt-106.jpg">
							<p>View Details</p>
						</a>
					</li><li>
						<a href="#">
							<img src="img/shirts/shirt-105.jpg">
							<p>View Details</p>
						</a>
					</li>								
				</ul>

			</div>

		</div>

	</div>

<?php include("inc/footer.php"); ?>	

</body>
</html>